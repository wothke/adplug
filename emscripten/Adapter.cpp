/*
* This file adapts "adplug" to the interface expected by my generic JavaScript _player..
*
* Copyright (C) 2014 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <stdio.h>
#include <emscripten.h>
#include <stdint.h>

#include "../src/adplug.h"

// there are different OPL emulators available in AdPlug: old ones like the
// Mame impl, Ken Silverman's or Tatsuyuki Satoh's impls, and newer ones:
// WoodyOPL is from 2016 and NukedOPL3 from 2018. The last two are probably
// the best ones and NukedOPL3 is apparently based on actual sound chip reverse
// engineering.

// so far I only patched the WoodyOPL to support scope-data streams for display
// purposes - in order to switch to NukedOPL3, respective add-ons would there
// first have to be implemented (see BufferPlayer).

//#include "../src/nemuopl.h"		// CNemuopl: Nuked OPL3 emulator
#include "../src/wemuopl.h"			// CWemuopl: new WoodyOPL


#include "BufferPlayer.h"
#include "MetaInfoHelper.h"

#define NUM_MAX		10

using emsutil::MetaInfoHelper;

namespace adplug {

class Adapter {
public:
	Adapter() {
		_opl = 0;
		_player = 0;
		_isReady = false;
		_playTime = _totalTime = _sampleRate = 0;

		_endless = false;
		_loops = _frameCount = _lengthInFrames= 0;
		_maxLoops = 1;
	}

	void teardown() {
	  _isReady = false;
	  _skipSilence = true;

	  if(_player) { delete _player; _player = 0; }
	  if(_opl) { delete _opl; _opl = 0; }
	}

	int loadFile(char *filename, uint32_t sampleRate, uint32_t audioBufSize) {
		_opl = new CWemuopl(sampleRate, true, true);

		_player = new BufferPlayer(_opl, 16, 2, sampleRate, audioBufSize);
		_opl->init();
		_player->p = CAdPlug::factory(std::string(filename), _opl);

		if (CAdPlug::isWaiting4Fileload(_player->p))
		{
			_player->p = 0;
			return -1;	// try again later
		}

		if(!_player->p)
		{
			delete _opl;
			_opl = 0;
			return 1;
		}

		_sampleRate = sampleRate;

		return 0;
	}

	int getSampleRate() {
		return _sampleRate;
	}

	void setOptions(int endless, int loops) {
		_endless = endless;
		_maxLoops = loops;
	}

	int setSubsong(int subsong) {
		_playTime = 0;
		_totalTime = _player->setSubsong(subsong);
		_loops = 0;
		_isReady = true;

		return 0;
	}

	const char** getSubsongInfo() {
		static char tmp[NUM_MAX];

		MetaInfoHelper *info = MetaInfoHelper::getInstance();
		info->clear();

		if (_player && _player->p)
		{
			info->setText(0, _player->p->gettitle().c_str(), "");
			info->setText(1, _player->p->getauthor().c_str(), "");
			info->setText(2, _player->p->getdesc().c_str(), "");
			info->setText(3, _player->p->gettype().c_str(), "");

			snprintf(tmp, NUM_MAX, "%d", _player->p->getspeed());
			info->setText(4, tmp, "");
			snprintf(tmp, NUM_MAX, "%d", _player->p->getsubsongs());
			info->setText(5, tmp, "");
		}
		return info->getMeta();
	}

	char* getAudioBuffer() {
		return (char*)_player->getSampleBuffer();
	}

	int32_t getAudioBufferLength() {
		return _player->getSampleBufferSize() >> 2;
	}

	int genSamples() {
		if (!_isReady) return 0;		// don't trigger a new "song end" while still initializing

		_player->frame();

		++_frameCount;

		// "playing" is cleared when the song end is first reached and used here to determine the
		// "length" in frames of looping once.

		if (!_player->playing)
		{
			if (_lengthInFrames == 0)
			{
				_lengthInFrames = _frameCount;
			}
			if (_frameCount >= _lengthInFrames)
			{
				++_loops;
				_frameCount = 0;
			}
		}

		int ret = _player->getSampleBufferSize()>>2;
		_playTime+= ret;

		if (_skipSilence) 	// see long silence at start of "inca" songs
		{
			 for (int i = 0; i < 10; i++) {
				 // limit the skipping performed in one call to limit exessive
				 // CPU load of one call (which might be an issue for browser
				 // responsiveness)

				 if (!isSilence())
				 {
					 _skipSilence = false;
					 break;
				 }

				_player->frame();
				ret = _player->getSampleBufferSize()>>2;
				_playTime += ret;
			 }
		}

		if (_endless || (_loops < _maxLoops))
		{
			return 0;
		}
		return 1;
	}

		// --- playback position

	int getCurrentPosition() {
		return _isReady ? _playTime / _sampleRate *1000 : -1;
	}

	void seekPosition(int pos) {
		_playTime = pos / 1000 * _sampleRate;
		_player->p->seek(pos);
	}

	int getMaxPosition() {
		return _isReady ? _totalTime : -1;
	}

		// --- scope data streams (only implemented for WoodyOPL emulator!)

	int getNumberTraceStreams() {
		return woody_scope_channels();
	}

	// the "percussion" input channels (6..8) are always moved to the very back and if "percussion mode"
	// is on, then these 3 input channels are replaced by 5 pseudo instrument channels (e.g. there will be
	// 11 instead of the original 9 OPL channels)
	const char** getTraceStreams() {
		return _isReady ? (const char**)_player->getScopeBuffers() : (const char**)0;
	}

	int* currentBufferPos() {
		return _isReady ? _player->currentBufferPos() : 0;
	}

		// --- extra info

	int getNumInsts() {
		return (_player && _player->p) ? _player->p->getinstruments() : 0;
	}

	const char* getInstText(unsigned int idx) {
		return (_player && _player->p) ? _player->p->getinstrument(idx).c_str() : "";
	}
private:
	bool isSilence() {
		int16_t* sampleBuffer = (int16_t*)getAudioBuffer();
		int size = _player->getSampleBufferSize()>>1;	// check both channels

		for (int i = 0; i < size; i++) {
			if(sampleBuffer[i]) return false;
		}
		return true;
	}
private:
	Copl *_opl;
	BufferPlayer *_player;
	bool _isReady;
	bool _skipSilence;
	uint32_t _playTime;
	uint32_t _totalTime;
	uint32_t _sampleRate;

	bool _endless;
	uint32_t _frameCount, _lengthInFrames;
	uint32_t _loops;
	uint32_t _maxLoops;
};
};

static adplug::Adapter _adapter;


extern "C" int*  emu_current_buffer_pos() {	// see woodyopl.cpp
	return _adapter.currentBufferPos();
}


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard APIs
EMBIND(int, emu_compute_audio_samples())				{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())					{ return _adapter.getAudioBuffer(); }
EMBIND(int32_t, emu_get_audio_buffer_length())			{ return _adapter.getAudioBufferLength(); }
EMBIND(int, emu_get_sample_rate())						{ return _adapter.getSampleRate(); }
EMBIND(int, emu_get_max_position())						{ return _adapter.getMaxPosition(); }
EMBIND(int, emu_get_current_position())					{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int pos))				{ _adapter.seekPosition(pos); }
EMBIND(int, emu_load_file(char *filename, void* inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled))	{
	// AdPlug loads file via name and input binary is unused, also scope output is always enabled
	return _adapter.loadFile(filename, sampleRate, audioBufSize); }
EMBIND(int, emu_set_subsong(int subsong))	{ return _adapter.setSubsong(subsong); }
EMBIND(void, emu_teardown())							{ _adapter.teardown(); }
EMBIND(int, emu_number_trace_streams())					{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())			{ return _adapter.getTraceStreams(); }
EMBIND(const char**, emu_get_trace_titles())			{ return 0; }
EMBIND(const char**, emu_get_track_info())				{ return _adapter.getSubsongInfo(); }


// --- add-on APIs
EMBIND(int, emu_get_num_insts())						{ return _adapter.getNumInsts(); }
EMBIND(const char*, emu_get_inst_text(uint32_t idx))	{ return _adapter.getInstText(idx); }

EMBIND(void, emu_set_options(int endless, int loops))	{ _adapter.setOptions(endless, loops); }
