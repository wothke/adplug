::  POOR MAN'S DOS PROMPT BUILD SCRIPT.. make sure to delete the respective built/*.bc files before building!
::  existing *.bc files will not be recompiled. 

setlocal enabledelayedexpansion

SET ERRORLEVEL
VERIFY > NUL

:: **** use the "-s WASM" switch to compile WebAssembly output. warning: the SINGLE_FILE approach does NOT currently work in Chrome 63.. ****  
set "OPT= -s WASM=1  -s DEMANGLE_SUPPORT=0 -s SAFE_HEAP=0 -s VERBOSE=0 -s ASSERTIONS=0 -s FORCE_FILESYSTEM=1 -s TOTAL_MEMORY=33554432 -Wno-pointer-sign -Dstricmp=strcasecmp -DVERSION="\"2.2.1-webaudio\"" -I. -I../src/ -I../libbinio/  -Os -O3 "

:: only using (see adapter.cpp) the WoodyOPL so there is really no need to compile the alternative impls:
::XXX  ../src/nukedopl.c ../src/nemuopl.cpp ../src/emuopl.cpp ../src/fmopl.c

if not exist "built/emus.bc" (
	call emcc.bat %OPT% ../libbinio/binwrap.cpp ../libbinio/binstr.cpp ../libbinio/binio.cpp ../libbinio/binfile.cpp ../src/debug.c ../src/pis.cpp ../src/adlibemu.c ../src/a2m.cpp ../src/adl.cpp ../src/adplug.cpp ../src/adtrack.cpp ../src/amd.cpp ../src/bam.cpp ../src/bmf.cpp ../src/cff.cpp ../src/cmf.cpp ../src/cmfmcsop.cpp ../src/coktel.cpp ../src/composer.cpp ../src/d00.cpp ../src/database.cpp ../src/dfm.cpp ../src/dmo.cpp ../src/dro.cpp ../src/dro2.cpp ../src/dtm.cpp ../src/flash.cpp ../src/fmc.cpp ../src/fprovide.cpp ../src/got.cpp ../src/herad.cpp ../src/hsc.cpp ../src/hsp.cpp ../src/hybrid.cpp ../src/hyp.cpp ../src/imf.cpp ../src/jbm.cpp ../src/ksm.cpp ../src/lds.cpp ../src/mad.cpp ../src/mdi.cpp ../src/mid.cpp ../src/mkj.cpp ../src/msc.cpp ../src/mtr.cpp ../src/mtk.cpp ../src/mus.cpp ../src/player.cpp ../src/players.cpp ../src/protrack.cpp ../src/psi.cpp ../src/rad2.cpp ../src/rat.cpp ../src/raw.cpp ../src/rix.cpp ../src/rol.cpp ../src/s3m.cpp ../src/sa2.cpp ../src/sng.cpp ../src/sop.cpp ../src/u6m.cpp ../src/vgm.cpp ../src/woodyopl.cpp ../src/xad.cpp ../src/xsm.cpp -o built/emus.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
emcc.bat %OPT% --closure 1 --llvm-lto 1 --memory-init-file 0  built/emus.bc BufferPlayer.cpp MetaInfoHelper.cpp Adapter.cpp --js-library callback.js -s EXPORTED_FUNCTIONS="['_emu_load_file','_emu_teardown','_emu_set_subsong','_emu_set_options','_emu_get_sample_rate','_emu_get_track_info','_emu_get_audio_buffer','_emu_get_audio_buffer_length','_emu_compute_audio_samples','_emu_get_current_position','_emu_seek_position','_emu_get_max_position','_emu_get_num_insts','_emu_get_inst_text','_emu_number_trace_streams','_emu_get_trace_streams','_emu_get_trace_titles', '_malloc', '_free']" -o htdocs/adplug.js -s SINGLE_FILE=0 -s EXTRA_EXPORTED_RUNTIME_METHODS="['ccall', 'Pointer_stringify', 'UTF8ToString', 'getValue']"  -s BINARYEN_ASYNC_COMPILATION=1 -s BINARYEN_TRAP_MODE='clamp' && copy /b shell-pre.js + htdocs\adplug.js + shell-post.js htdocs\adplug3.js && del htdocs\adplug.js && copy /b htdocs\adplug3.js + adplug_adapter.js htdocs\backend_adplug.js && del htdocs\adplug3.js