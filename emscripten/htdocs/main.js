
let songs = [

	"Ad Lib/Mlat Adlib Tracker/Mem/kostky.mad;;194",
	"Ad Lib/AdLib Tracker 2/Malfunction/khaos 2 (4kb intro zak).a2m",
	"Ad Lib/Loudness Sound System/- unknown/cold dreams - coldint.lds",
	"Ad Lib/HSC AdLib Composer/Hannes Seifert/$pleen/connect.hsc",
	"Ad Lib/Exotic AdLib/Shadowdancer/eiserne front bbstro tune.xad",
	"Ad Lib/Twin TrackPlayer/Benjamin Gerardin/global alternative trance.dmo",
	"Ad Lib/Twin TrackPlayer/Benjamin Gerardin/la boite speciale daubes.dmo",
	"Ad Lib/AMusic/Phandral/lightforce.amd",
	"Ad Lib/Exotic AdLib/Rogue/lunatic asylum bbstro tune.xad",
	"Ad Lib/AMusic/Hydra/mitrax.amd",
	"Ad Lib/AMusic XMS/- unknown/neo intro tune in fm.xms",
	"Ad Lib/Raw OPL Capture/- unknown/pentagram.raw",
	];


class AnimationSequence
{
	constructor(durationSecs, numBlobs, blobSize)
	{
		this._durationMs = durationSecs * 1000;	// in secs
		this._fadeDurationMs = 300;

		this._numBlobs = numBlobs;
		this._blobCoords = new Float32Array(this._numBlobs * 2);
		this._blobSize = blobSize;

	}

	getBlobSize()
	{
		return this._blobSize;
	}

	getBlobCoords()
	{
		return this._blobCoords;
	}

	start()
	{
		this._startMs = performance.now();
		this._endMs = this._startMs + this._durationMs;
	}

	isDone()
	{
		return performance.now() >= this._endMs;
	}

	getNumBlobs()
	{
		return this._numBlobs;
	}

	getFadeIn()
	{
		let t = performance.now();
		let d = t - this._startMs;

		if (d < this._fadeDurationMs)
		{
			return d / this._fadeDurationMs;
		}
		else if (t > (this._endMs - this._fadeDurationMs))
		{
			return (this._endMs - t) / this._fadeDurationMs;
		}
		return 1;
	}

	updateBlobCoords() {
		let timeSecs =  (performance.now() - this._startMs) / 1000;
		this._updateBlobCoords(timeSecs);
	}

	_updateBlobCoords(time) {}
}


class AnimationSequence1 extends AnimationSequence {		// "heart"
	constructor(duration, scale)
	{
		super(duration, 100, 0.07);
		this._scale = scale;
	}

	_updateBlobCoords(time)
	{
		let a = time / 4;
		for (let i = 0, j = 0; i<this._numBlobs; i++) {
			let t = i * 2 * Math.PI / this._numBlobs;

			this._blobCoords[j++] = 16*Math.pow(Math.sin(t*a), 3) * this._scale;
			this._blobCoords[j++] = (13*Math.cos(t*a)* Math.atan(a)*0.8 - (5*Math.cos(2*t*a) + 2*Math.cos(3*t*a) + Math.cos(4*t*a)) * Math.sin(a) ) * this._scale;
		}
	}
}

class AnimationSequence2 extends AnimationSequence {
	constructor(duration, scale)
	{
		super(duration, 200, 0.06);
		this._scale = scale;
	}

	_updateBlobCoords(time)
	{
		let a = time / 4;

		for (let i = 0, j = 0; i<this._numBlobs; i++) {
			let t = i * 2 * Math.PI / this._numBlobs;

			this._blobCoords[j++] = Math.sin(Math.sin(t)*a) * this._scale;
			this._blobCoords[j++] = Math.sin(Math.cos(t)*a) * this._scale;
		}
	}
}

class AnimationSequence3 extends AnimationSequence {
	constructor(duration, scale)
	{
		super(duration, 100, 0.07);
		this._scale = scale;
	}

	_updateBlobCoords(time)
	{
		let a = time / 4;

		for (let i = 0, j = 0; i<this._numBlobs; i++) {
			let t = i * 2 * Math.PI / this._numBlobs;

			this._blobCoords[j++] = Math.sin(t) * this._scale * Math.cos(t*a);
			this._blobCoords[j++] = Math.cos(t) * this._scale * Math.cos(t*a);
		}
	}
}

class AnimationSequence4 extends AnimationSequence {
	constructor(duration, scale)
	{
		super(duration, 100, 0.07);
		this._scale = scale;
	}

	_updateBlobCoords(time)
	{
		let a = time / 1.5;

		for (let i = 0, j = 0; i<this._numBlobs; i++) {
			let t = i * 2 * Math.PI / this._numBlobs;

			this._blobCoords[j++] = Math.sin(t) * this._scale + Math.cos(t*a)*3;
			this._blobCoords[j++] = Math.cos(t) * this._scale + Math.sin(t*a)*3;
		}
	}
}


class PerfLogger {
	constructor()
	{
		this._enableLog = false;

		this._runs = 50;
		this._sum = 0;
		this._count = 0;
	}

	startFrame()
	{
		this._start = performance.now();
	}

	endFrame()
	{
		this._sum += performance.now() - this._start;

		if (this._enableLog && (++this._count >= this._runs))
		{
			console.log(this._sum / this._runs);

			this._sum = 0;
			this._count = 0;
		}
	}
}

class AdLibPlaylistPlayerWidget extends PlaylistPlayerWidget {
	constructor(containerId, songs, onNewTrackCallback, enableSeek, enableSpeedTweak, doParseUrl, doOnDropFile, currentTrack, defaultOptions)
	{
		super(containerId, songs, onNewTrackCallback, enableSeek, enableSpeedTweak, doParseUrl, doOnDropFile, currentTrack, defaultOptions);
	}

	_addSong(filename)
	{
		if ((filename.indexOf(".003") == -1) && (filename.indexOf(".inst") == -1) && (filename.indexOf(".bnk") == -1))
		{
			super._addSong(filename);
		}
	}
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._scopesWidget;

		this._blobShaderCache = {};

		this._perfLogger = new PerfLogger();

		this._animIdx = 0;
		this._animSequences = [
			new AnimationSequence1(10, 0.5),
			new AnimationSequence2(34, 10),
			new AnimationSequence4(150, 6),
			new AnimationSequence3(240, 10),
		];
		this._animationSequence = null;
	}

	_startNextAnimationSequence()
	{
		this._animationSequence = this._animSequences[this._animIdx++ % this._animSequences.length];
		this._animationSequence.start();
	}

	_getAnimationSequence()
	{
		if (this._animationSequence == null || this._animationSequence.isDone())
		{
			this._startNextAnimationSequence();
		}
		return this._animationSequence;
	}


	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_setupSliderHoverState()
	{
		// hack: add "highlight while dragging" feature for all sliders on the page
		// (this should rather be setup locally upon creation of the sliders..)
		let slider = $(".slider a.ui-slider-handle");

		slider.hover(function() {
				$(this).prev().toggleClass('hilite');
			});

		slider.mousedown(function() {
				$(this).prev().addClass('dragging');
				$("*").mouseup(function() {
					$(slider).prev().removeClass('dragging');
				});
			});
	}

	getVuMeterLevel()
	{
		if (typeof this._channelStreamer == 'undefined')
		{
			return 0;
		}
		return this._channelStreamer.getOverallVuMeterLevel();
	}

	_createProgram(fragShader) {
		let p = setupProgram(window.shaders.vertex, fragShader);

		// always use same vertex shader..

		p.vertexPosAttrib = gl.getAttribLocation(p, 'aVertexPosition');
		gl.enableVertexAttribArray(p.vertexPosAttrib);
		gl.vertexAttribPointer(p.vertexPosAttrib, this._vertexPosBuffer.itemSize, gl.FLOAT, false, 0, 0);

		return p;
	}

	_useProgram(p)
	{
		gl.useProgram(p);
		return p;
	}

	_renderFrameBuffer()
	{
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, this._vertexPosBuffer.numItems);
	}

	getBlobShaderProg(numBlobs)
	{
		let key = "" + numBlobs;
		if (!this._blobShaderCache.hasOwnProperty(key))
		{
			let blobFragment = window.shaders.blobFragment.replaceAll("NUM_BLOBS", numBlobs);
			this._blobShaderCache[key] = this._createProgram(blobFragment);
		}
		return this._blobShaderCache[key];
	}

	draw()
	{
		let time = performance.now() / 1000;	// secs
		time = time % 900;						// reset every 15 minutes

		let anim = this._getAnimationSequence();
		anim.updateBlobCoords();

		let fadeIn = anim.getFadeIn();
		let numBlobs = anim.getNumBlobs();
		let blobSize = anim.getBlobSize();

		let light_x	= -15 * Math.cos(time);
		let light_y	= 12 * Math.cos(time * 0.4);
		let light_z	= 5;

		let c = document.getElementById('c');
		let width =  c.width;
		let height =  c.height;


		this._perfLogger.startFrame();

		// 1: render blobs
		this.programBlob = this.getBlobShaderProg(numBlobs);

		let p = this._useProgram(this.programBlob);

		activateTextureCubeMap(0, this._cubeTexture);
		activateTexture2d(1, null);
		activateTexture2d(2, null);

		setFrameBuffer(gl.COLOR_ATTACHMENT0, this._bufferA, width, height);

		gl.uniform2f(gl.getUniformLocation(p, 'uCanvasSize'), width, height);
		gl.uniform1f(gl.getUniformLocation(p, 'uTime'), time);
		gl.uniform1f(gl.getUniformLocation(p, 'uBlobSize'), anim.getBlobSize());
		gl.uniform3f(gl.getUniformLocation(p, 'uLightPos'), light_x, light_y, light_z);
		gl.uniform2fv(gl.getUniformLocation(p, 'uBlobCoords'), anim.getBlobCoords());
		gl.uniform1i(gl.getUniformLocation(p, "texCubemap"), 0);

		this._renderFrameBuffer();


	// 2: render blurred version for shadows

		p = this._useProgram(this._programBlur);

		activateTexture2d(0, this._bufferA);						// output from step 1
		activateTexture2d(1, null);
		activateTexture2d(2, null);

		setFrameBuffer(gl.COLOR_ATTACHMENT0, this._bufferB, width, height);

		gl.uniform2f(gl.getUniformLocation(p, 'uCanvasSize'), c.width, c.height);
		gl.uniform1i(gl.getUniformLocation(p, "inTexture"), 0);

		this._renderFrameBuffer();


	// 3: render final (merge blobs & background)

		setFrameBuffer(gl.COLOR_ATTACHMENT0, null);

		p = this._useProgram(this._programMerge);


		activateTexture2d(0, this._bgTexture);
		activateTexture2d(1, this._bufferA);
		activateTexture2d(2, this._bufferB);

		gl.uniform2f(gl.getUniformLocation(p, 'uCanvasSize'), width, height);
		gl.uniform1f(gl.getUniformLocation(p, 'uTime'), time);
		gl.uniform1f(gl.getUniformLocation(p, 'uFadeIn'), fadeIn);
		gl.uniform3f(gl.getUniformLocation(p, 'uLightPos'), light_x, light_y, light_z);
		gl.uniform1i(gl.getUniformLocation(p, "uBackgroundTexture"), 0);
		gl.uniform1i(gl.getUniformLocation(p, "uBlobsTexture"), 1);		// i.e. this._bufferA
		gl.uniform1i(gl.getUniformLocation(p, "uShadowTexture"), 2);		// i.e. this._bufferB

		this._renderFrameBuffer();

		this._perfLogger.endFrame();


		requestAnimationFrame(this.draw.bind(this))
	}

	_runAnimation()
	{
		let c = document.getElementById('c');

		if (useWEBGL(c))
		{
			let p1 = loadTexture("bg.jpg");

			// note: image #2 is the sky (#3 the ground) and produces the the most highlights
			// (the "ground" of the rendered scene however stands vertically which causes
			// image #5 to me its "sky" - i.e. the scene in the cubemap makes no sense
			// though it looks nice)

			let p2 = loadCubemap("5.webp", "5.webp", "5.webp", "5.webp", "5.webp", "5.webp");	// not enough added value in additional images
//			let p2 = loadCubemap("0.webp", "1.webp", "2.webp", "3.webp", "4.webp", "5.webp");

			Promise.all([p1, p2]).then((result) => {
				this._bgTexture = result[0];
				this._cubeTexture = result[1];


				this._vertexPosBuffer = screenQuad();

				this._bufferA = createTexture(c.width, c.height); // 1st rendering step: raw blobs
				this._bufferB = createTexture(c.width, c.height); // 2st rendering step: blurred for shadows

				this.getBlobShaderProg(100);	// pre-populate cache
				this.getBlobShaderProg(200);

				this._programBlur = this._createProgram(window.shaders.blurFragment);
				this._programMerge = this._createProgram(window.shaders.mergeFragment);


				this.draw();
			})
			.catch(err => {
				console.log(err);
			});
		}
	}

	// note: some async loading (e.g. WASM) may still be in progress at this point
	run()
	{
		this._runAnimation();


		this._channelStreamer = new ChannelStreamer();
		this._channelStreamer.setZoom(1);

		this._backend = new AdPlugBackendAdapter();
		this._backend.setProcessorBufSize(4096);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), [], false, this._channelStreamer)
		.then((msg) => {

			let defaultSongIdx = -1;

			// OscilloscopesWidget constructor requires funtional backend!
			this._scopesWidget = new OscilloscopesWidget("scopesContainer", this._channelStreamer, this._backend);

			let optionsParser = function(someSong) {
									let arr = someSong.split(";");
									if (arr.length > 0) someSong = arr[0];

									let options = {};

									let track = arr.length > 1 ? parseInt(arr[1]) : -1;
									options.track = track;

									let timeout = arr.length > 2 ? parseInt(arr[2]) : -1;
									options.timeout = timeout;

									let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
									someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

									return [someSong, options];
								};

			this._playerWidget = new AdLibPlaylistPlayerWidget("playerContainer", songs, this._scopesWidget.onSongChanged.bind(this._scopesWidget),
															false, true, optionsParser, 0, defaultSongIdx, {});

			// use after all widgets have been setup
			this._setupSliderHoverState();

			this._playerWidget.playNextSong();
		});
	}
}
