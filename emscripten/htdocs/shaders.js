// metaballs v0.2
// copyright (C) 2023 Juergen Wothke
//
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

window.shaders = window.shaders || {};

window.shaders.vertex = `#version 300 es
	in vec2 aVertexPosition;
	void main() {
		gl_Position = vec4(aVertexPosition, 0, 1);
	}
`;

let defines = `
	#define NEAR_Z 1.
	#define SHININESS 100.

//	#define NUM_BLOBS 100
	#define BLOB_Z -.9
//	#define BLOB_SIZE .07

	#define MAX_MARCHING_STEPS 50
	#define MARCHING_STEP .17
`;

let sharedFunc = `
	void init(in vec2 fragCoord, in vec2 resolution, out vec3 eye, out vec3 ray)
	{
		float 	ratio			= resolution.x/resolution.y;
		vec3 	viewportCoord	= vec3(fragCoord.xy/resolution.y - vec2(ratio *.5, .5), NEAR_Z);

		eye						= vec3(0., 0., BLOB_Z);
		ray						= normalize(viewportCoord - eye);
	}
`;

window.shaders.blobFragment = `#version 300 es
#ifdef GL_FRAGMENT_PRECISION_HIGH
	precision highp float;
#else
	precision mediump float;
#endif
	precision mediump int;
	
	` + defines + `
	
	uniform vec2 uCanvasSize;
	uniform vec3 uLightPos;
	uniform float uTime;
	uniform float uBlobSize;
	uniform vec2 uBlobCoords[NUM_BLOBS];
	uniform samplerCube texCubemap;
	
	out vec4 outputColor;
	
	` + sharedFunc + `
	
	
	vec3 getBallCenter(in float j) 
	{
		int i = int(j);
		return vec3(uBlobCoords[i].x, uBlobCoords[i].y, 48.);		
	}

	float calcMetaball(in float uTime, in vec3 surface, out vec3 normal) 
	{
		normal = vec3(0,0,0);
		float r = 0.0;

		for(float i = 0.0; i < float(NUM_BLOBS); i += 1.0) 
		{
			vec3 pos = getBallCenter(i);
			float d = length(surface - pos);

			float r0 = uBlobSize / (d*d);
			normal += normalize(surface - pos) * r0;
			r += r0;
		}

		normal = normalize(normal);
		return r;
	}

	float rayMarch(in float uTime, in vec3 origin, in vec3 ray, out vec3 surface, out vec3 normal) 
	{
		float	dist = 47.1 - BLOB_Z; 

		surface = origin;   
		
		for(int i = 0; i < MAX_MARCHING_STEPS; i++) 
		{      
			float r = calcMetaball(uTime, surface, normal);
			
			if(r > 0.5) 
			{
				return r;
			}
			else 
			{
				dist += MARCHING_STEP;
				surface = origin + ray * dist;    
			}
		}
		
		normal = vec3(0.,0.,1.);  // imaginary background plane
		return 0.;   
	}
		
	vec4 blobColor( in vec3 ray, in vec3 surface, in vec3 normal, in vec3 lightSrc ) 
	{			
		// image #5 is responsible for most of the "top" lighting (most of the bright 
		// spots come from that image..)
		vec3 color = texture(texCubemap, normal).xyz;

		vec3 lightDir = normalize(surface - lightSrc );
		float diffuse = dot(normal, -lightDir) * 2.4;
		
		vec3 reflected = reflect(lightDir, normal) ;
		
		float h = pow(max(dot(reflected, -ray), .0), SHININESS);
		vec3 highlight = vec3(h, h, h);
		
		vec3 ambient = vec3(1,0.3,0.1) * 0.4;        

		return vec4(color * diffuse + highlight + ambient, 1.0);
	}

	void main()
	{    
		vec3 eye, ray;       
		init(gl_FragCoord.xy, uCanvasSize, eye, ray);
		
		vec3 surface, normal; 	
		if(rayMarch(uTime, eye, ray, surface, normal) > 0.5) 
		{
			outputColor = blobColor(ray, surface, normal, uLightPos);
		}
		else 
		{    
			outputColor = vec4(0.,0.,0.,0.);
		}
	}	
	
`;

window.shaders.blurFragment = `#version 300 es
#ifdef GL_FRAGMENT_PRECISION_HIGH
	precision highp float;
#else
	precision mediump float;
#endif
	precision mediump int;
	
	uniform vec2 uCanvasSize;
	uniform sampler2D inTexture;
	
	out vec4 outputColor;
		
	void main()
	{
		float Pi = 6.28318530718; // Pi*2
		
		// GAUSSIAN BLUR SETTINGS {{{
		float Directions = 16.0; // BLUR DIRECTIONS (Default 16.0 - More is better but slower)
		float Quality = 3.0; // BLUR QUALITY (Default 4.0 - More is better but slower)
		float Size = 12.0; // BLUR SIZE (Radius)
		// GAUSSIAN BLUR SETTINGS }}}
	   
		vec2 Radius = Size/uCanvasSize.xy;
		
		// Normalized pixel coordinates (from 0 to 1)
		vec2 uv = gl_FragCoord.xy/uCanvasSize.xy;
		// Pixel colour
		vec4 Color = texture(inTexture, uv);
		
		// Blur calculations
		for( float d=0.0; d<Pi; d+=Pi/Directions)
		{
			for(float i=1.0/Quality; i<=1.0; i+=1.0/Quality)
			{
				Color += texture( inTexture, uv+vec2(cos(d),sin(d))*Radius*i);		
			}
		}
		
		// Output to screen
		Color /= Quality * Directions - 15.0;
		outputColor =  Color;
	}	
`;

window.shaders.mergeFragment = `#version 300 es
#ifdef GL_FRAGMENT_PRECISION_HIGH
	precision highp float;
#else
	precision mediump float;
#endif
	precision mediump int;
	
	uniform vec2 uCanvasSize;
	uniform vec3 uLightPos;
	uniform float uTime;
	uniform float uFadeIn;
	uniform sampler2D uBackgroundTexture;
	uniform sampler2D uBlobsTexture;
	uniform sampler2D uShadowTexture;
	
	out vec4 outputColor;
	
	` + defines + `
	` + sharedFunc + `
	

	// texture bumpmapping from: https://www.shadertoy.com/view/XdKSD3
	// Tri-Planar blending function. Based on an old Nvidia writeup:
	// GPU Gems 3 - Ryan Geiss: http://http.developer.nvidia.com/GPUGems3/gpugems3_ch01.html
	vec3 tex3D( sampler2D tex, in vec3 p, in vec3 n )
	{   
		n = max((abs(n) - .2) * 7., .001);
		n /= (n.x + n.y + n.z );  
		
		p = (texture(tex, p.yz) * n.x + texture(tex, p.zx) * n.y + texture(tex, p.xy) * n.z).xyz;
		
		return p * p;
	}

	// Texture bump mapping. Four tri-planar lookups, or 12 texture lookups in total. I tried to 
	// make it as concise as possible. Whether that translates to speed, or not, I couldn't say.
	vec3 bumpMap( sampler2D tx, in vec3 p, in vec3 n, float bf)
	{   
		const vec2 e = vec2(0.001, 0);
		
		// Three gradient vectors rolled into a matrix, constructed with offset greyscale texture values.    
		mat3 m = mat3( tex3D(tx, p - e.xyy, n), tex3D(tx, p - e.yxy, n), tex3D(tx, p - e.yyx, n));
		
		vec3 g = vec3(0.299, 0.587, 0.114) * m; // Converting to greyscale.
		g = (g - dot(tex3D(tx,  p , n), vec3(0.299, 0.587, 0.114)) )/e.x; g -= n * dot(n, g);
						  
		return normalize( n + g * bf ); // Bumped normal. "bf" - bump factor. 
	}

	float getShadow(in vec2 uv) 
	{
		vec2 base = uv + vec2(uLightPos.x / 4000., uLightPos.y / 4000.);
		
		return 1.0 -  texture(uShadowTexture, base).x * 1.7;
	}

	float getGlow(in vec2 uv) 
	{
		vec2 base = uv - vec2(uLightPos.x / 2000., uLightPos.y / 2000.);
		
		return texture(uShadowTexture, base).x * 0.15;
	}

	vec4 colorBackground(in vec2 uv, vec3 eye, in vec3 ray, in vec3 light) 
	{
		vec3 color = texture(uBackgroundTexture, uv).xyz;
		vec3 normal = bumpMap(uBackgroundTexture, vec3(uv.x, uv.y, 10.)*1.2, vec3(0.,0.,1.), 0.002);    
		
		// replicate rayMarch result for background plane
		float marchingDist = 47.1 - BLOB_Z;
		vec3 surface = eye + ray *  marchingDist + MARCHING_STEP * float(MAX_MARCHING_STEPS);    

		// apply lighing
		vec3 reflectedLight  = reflect(normalize(surface - light ), normal) ;

		color = mix(color, vec3(0,0,0), 1.0 - getShadow(uv) );    

		color += vec3(1., .88, .48) *  getGlow(uv); 
		
		vec3 hilight = vec3(0.2, 0.2, 0.2) * pow(max(dot(reflectedLight, -ray),0.0), SHININESS); 
		vec3 ambiant = vec3(0.2,0.2,0.1) * 0.3;        

		return vec4(color * 0.8 + hilight + ambiant, 1.0);
	}

	void main()
	{    
		vec3 eye, ray;       
		init(gl_FragCoord.xy, uCanvasSize, eye, ray);
			
		vec2 uv = gl_FragCoord.xy / uCanvasSize.xy;
		outputColor = texture(uBlobsTexture, uv);
		
		if (outputColor.a == 0.) 
		{
			outputColor = colorBackground(uv, eye, ray, uLightPos);
		} 
		outputColor.rgb *= uFadeIn;
	}	
`;


