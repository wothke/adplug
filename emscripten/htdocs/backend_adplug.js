// create separate namespace for all the Emscripten stuff.. otherwise naming clashes may occur especially when 
// optimizing using closure compiler..
window.spp_backend_state_ADPLUG= {
	locateFile: function(path, scriptDirectory) { return (typeof window.WASM_SEARCH_PATH == 'undefined') ? path : window.WASM_SEARCH_PATH + path; },
	notReady: true,
	adapterCallback: function(){}	// overwritten later
};
window.spp_backend_state_ADPLUG["onRuntimeInitialized"] = function() {	// emscripten callback needed in case async init is used (e.g. for WASM)
	this.notReady= false;
	this.adapterCallback();
}.bind(window.spp_backend_state_ADPLUG);

var backend_AdPlug = (function(Module) {var e;e||(e=typeof Module !== 'undefined' ? Module : {});var k={},l;for(l in e)e.hasOwnProperty(l)&&(k[l]=e[l]);e.arguments=[];e.thisProgram="./this.program";e.quit=function(a,b){throw b;};e.preRun=[];e.postRun=[];var n=!1,q=!1,r=!1,aa=!1;n="object"===typeof window;q="function"===typeof importScripts;r="object"===typeof process&&"function"===typeof require&&!n&&!q;aa=!n&&!r&&!q;var t="";function ca(a){return e.locateFile?e.locateFile(a,t):t+a}
if(r){t=__dirname+"/";var da,ea;e.read=function(a,b){da||(da=require("fs"));ea||(ea=require("path"));a=ea.normalize(a);a=da.readFileSync(a);return b?a:a.toString()};e.readBinary=function(a){a=e.read(a,!0);a.buffer||(a=new Uint8Array(a));assert(a.buffer);return a};1<process.argv.length&&(e.thisProgram=process.argv[1].replace(/\\/g,"/"));e.arguments=process.argv.slice(2);"undefined"!==typeof module&&(module.exports=e);process.on("uncaughtException",function(a){throw a;});process.on("unhandledRejection",
u);e.quit=function(a){process.exit(a)};e.inspect=function(){return"[Emscripten Module object]"}}else if(aa)"undefined"!=typeof read&&(e.read=function(a){return read(a)}),e.readBinary=function(a){if("function"===typeof readbuffer)return new Uint8Array(readbuffer(a));a=read(a,"binary");assert("object"===typeof a);return a},"undefined"!=typeof scriptArgs?e.arguments=scriptArgs:"undefined"!=typeof arguments&&(e.arguments=arguments),"function"===typeof quit&&(e.quit=function(a){quit(a)});else if(n||q)q?
t=self.location.href:document.currentScript&&(t=document.currentScript.src),t=0!==t.indexOf("blob:")?t.substr(0,t.lastIndexOf("/")+1):"",e.read=function(a){var b=new XMLHttpRequest;b.open("GET",a,!1);b.send(null);return b.responseText},q&&(e.readBinary=function(a){var b=new XMLHttpRequest;b.open("GET",a,!1);b.responseType="arraybuffer";b.send(null);return new Uint8Array(b.response)}),e.readAsync=function(a,b,c){var d=new XMLHttpRequest;d.open("GET",a,!0);d.responseType="arraybuffer";d.onload=function(){200==
d.status||0==d.status&&d.response?b(d.response):c()};d.onerror=c;d.send(null)},e.setWindowTitle=function(a){document.title=a};var fa=e.print||("undefined"!==typeof console?console.log.bind(console):"undefined"!==typeof print?print:null),w=e.printErr||("undefined"!==typeof printErr?printErr:"undefined"!==typeof console&&console.warn.bind(console)||fa);for(l in k)k.hasOwnProperty(l)&&(e[l]=k[l]);k=void 0;function ha(a){var b=x;x=x+a+15&-16;return b}
function ia(a){var b;b||(b=16);return Math.ceil(a/b)*b}var ja={"f64-rem":function(a,b){return a%b},"debugger":function(){debugger}},ka=!1;function assert(a,b){a||u("Assertion failed: "+b)}var qa={stackSave:function(){la()},stackRestore:function(){ma()},arrayToC:function(a){var b=na(a.length);y.set(a,b);return b},stringToC:function(a){var b=0;if(null!==a&&void 0!==a&&0!==a){var c=(a.length<<2)+1;b=na(c);oa(a,z,b,c)}return b}},ra={string:qa.stringToC,array:qa.arrayToC};
function A(a,b){if(0===b||!a)return"";for(var c=0,d,f=0;;){d=z[a+f>>0];c|=d;if(0==d&&!b)break;f++;if(b&&f==b)break}b||(b=f);d="";if(128>c){for(;0<b;)c=String.fromCharCode.apply(String,z.subarray(a,a+Math.min(b,1024))),d=d?d+c:c,a+=1024,b-=1024;return d}return sa(a)}var ta="undefined"!==typeof TextDecoder?new TextDecoder("utf8"):void 0;
function C(a,b){for(var c=b;a[c];)++c;if(16<c-b&&a.subarray&&ta)return ta.decode(a.subarray(b,c));for(c="";;){var d=a[b++];if(!d)return c;if(d&128){var f=a[b++]&63;if(192==(d&224))c+=String.fromCharCode((d&31)<<6|f);else{var g=a[b++]&63;if(224==(d&240))d=(d&15)<<12|f<<6|g;else{var h=a[b++]&63;if(240==(d&248))d=(d&7)<<18|f<<12|g<<6|h;else{var m=a[b++]&63;if(248==(d&252))d=(d&3)<<24|f<<18|g<<12|h<<6|m;else{var p=a[b++]&63;d=(d&1)<<30|f<<24|g<<18|h<<12|m<<6|p}}}65536>d?c+=String.fromCharCode(d):(d-=
65536,c+=String.fromCharCode(55296|d>>10,56320|d&1023))}}else c+=String.fromCharCode(d)}}function sa(a){return C(z,a)}
function oa(a,b,c,d){if(!(0<d))return 0;var f=c;d=c+d-1;for(var g=0;g<a.length;++g){var h=a.charCodeAt(g);if(55296<=h&&57343>=h){var m=a.charCodeAt(++g);h=65536+((h&1023)<<10)|m&1023}if(127>=h){if(c>=d)break;b[c++]=h}else{if(2047>=h){if(c+1>=d)break;b[c++]=192|h>>6}else{if(65535>=h){if(c+2>=d)break;b[c++]=224|h>>12}else{if(2097151>=h){if(c+3>=d)break;b[c++]=240|h>>18}else{if(67108863>=h){if(c+4>=d)break;b[c++]=248|h>>24}else{if(c+5>=d)break;b[c++]=252|h>>30;b[c++]=128|h>>24&63}b[c++]=128|h>>18&63}b[c++]=
128|h>>12&63}b[c++]=128|h>>6&63}b[c++]=128|h&63}}b[c]=0;return c-f}"undefined"!==typeof TextDecoder&&new TextDecoder("utf-16le");var buffer,y,z,ua,D,va,wa;function xa(){e.HEAP8=y=new Int8Array(buffer);e.HEAP16=ua=new Int16Array(buffer);e.HEAP32=D=new Int32Array(buffer);e.HEAPU8=z=new Uint8Array(buffer);e.HEAPU16=new Uint16Array(buffer);e.HEAPU32=new Uint32Array(buffer);e.HEAPF32=va=new Float32Array(buffer);e.HEAPF64=wa=new Float64Array(buffer)}var ya,x,za,Aa,Ba,Ca,Da,E;ya=x=Aa=Ba=Ca=Da=E=0;za=!1;
function Ea(){u("Cannot enlarge memory arrays. Either (1) compile with  -s TOTAL_MEMORY=X  with X higher than the current value "+F+", (2) compile with  -s ALLOW_MEMORY_GROWTH=1  which allows increasing the size at runtime, or (3) if you want malloc to return NULL (0) instead of this abort, compile with  -s ABORTING_MALLOC=0 ")}var Fa=e.TOTAL_STACK||5242880,F=e.TOTAL_MEMORY||33554432;F<Fa&&w("TOTAL_MEMORY should be larger than TOTAL_STACK, was "+F+"! (TOTAL_STACK="+Fa+")");
e.buffer?buffer=e.buffer:("object"===typeof WebAssembly&&"function"===typeof WebAssembly.Memory?(e.wasmMemory=new WebAssembly.Memory({initial:F/65536,maximum:F/65536}),buffer=e.wasmMemory.buffer):buffer=new ArrayBuffer(F),e.buffer=buffer);xa();function Ga(a){for(;0<a.length;){var b=a.shift();if("function"==typeof b)b();else{var c=b.Da;"number"===typeof c?void 0===b.V?e.dynCall_v(c):e.dynCall_vi(c,b.V):c(void 0===b.V?null:b.V)}}}var Ha=[],Ia=[],Ja=[],Ka=[],La=[],Oa=!1;
function Pa(){var a=e.preRun.shift();Ha.unshift(a)}var G=0,Qa=null,Ra=null;function Sa(){G++;e.monitorRunDependencies&&e.monitorRunDependencies(G)}function Ta(){G--;e.monitorRunDependencies&&e.monitorRunDependencies(G);if(0==G&&(null!==Qa&&(clearInterval(Qa),Qa=null),Ra)){var a=Ra;Ra=null;a()}}e.preloadedImages={};e.preloadedAudios={};function Ua(a){return String.prototype.startsWith?a.startsWith("data:application/octet-stream;base64,"):0===a.indexOf("data:application/octet-stream;base64,")}
(function(){function a(){try{if(e.wasmBinary)return new Uint8Array(e.wasmBinary);if(e.readBinary)return e.readBinary(f);throw"both async and sync fetching of the wasm failed";}catch(B){u(B)}}function b(){return e.wasmBinary||!n&&!q||"function"!==typeof fetch?new Promise(function(b){b(a())}):fetch(f,{credentials:"same-origin"}).then(function(a){if(!a.ok)throw"failed to load wasm binary file at '"+f+"'";return a.arrayBuffer()}).catch(function(){return a()})}function c(a){function c(a){m=a.exports;if(m.memory){a=
m.memory;var b=e.buffer;a.byteLength<b.byteLength&&w("the new buffer in mergeMemory is smaller than the previous one. in native wasm, we should grow memory here");b=new Int8Array(b);(new Int8Array(a)).set(b);e.buffer=buffer=a;xa()}e.asm=m;e.usingWasm=!0;Ta()}function d(a){c(a.instance)}function g(a){b().then(function(a){return WebAssembly.instantiate(a,h)}).then(a,function(a){w("failed to asynchronously prepare wasm: "+a);u(a)})}if("object"!==typeof WebAssembly)return w("no native wasm support detected"),
!1;if(!(e.wasmMemory instanceof WebAssembly.Memory))return w("no native wasm Memory in use"),!1;a.memory=e.wasmMemory;h.global={NaN:NaN,Infinity:Infinity};h["global.Math"]=Math;h.env=a;Sa();if(e.instantiateWasm)try{return e.instantiateWasm(h,c)}catch(J){return w("Module.instantiateWasm callback failed with error: "+J),!1}e.wasmBinary||"function"!==typeof WebAssembly.instantiateStreaming||Ua(f)||"function"!==typeof fetch?g(d):WebAssembly.instantiateStreaming(fetch(f,{credentials:"same-origin"}),h).then(d,
function(a){w("wasm streaming compile failed: "+a);w("falling back to ArrayBuffer instantiation");g(d)});return{}}var d="adplug.wast",f="adplug.wasm",g="adplug.temp.asm.js";Ua(d)||(d=ca(d));Ua(f)||(f=ca(f));Ua(g)||(g=ca(g));var h={global:null,env:null,asm2wasm:ja,parent:e},m=null;e.asmPreload=e.asm;var p=e.reallocBuffer;e.reallocBuffer=function(a){if("asmjs"===v)var b=p(a);else a:{var c=e.usingWasm?65536:16777216;0<a%c&&(a+=c-a%c);c=e.buffer.byteLength;if(e.usingWasm)try{b=-1!==e.wasmMemory.grow((a-
c)/65536)?e.buffer=e.wasmMemory.buffer:null;break a}catch(pa){b=null;break a}b=void 0}return b};var v="";e.asm=function(a,b){if(!b.table){a=e.wasmTableSize;void 0===a&&(a=1024);var d=e.wasmMaxTableSize;b.table="object"===typeof WebAssembly&&"function"===typeof WebAssembly.Table?void 0!==d?new WebAssembly.Table({initial:a,maximum:d,element:"anyfunc"}):new WebAssembly.Table({initial:a,element:"anyfunc"}):Array(a);e.wasmTable=b.table}b.memoryBase||(b.memoryBase=e.STATIC_BASE);b.tableBase||(b.tableBase=
0);b=c(b);assert(b,"no binaryen method succeeded.");return b}})();ya=1024;x=ya+59888;Ia.push({Da:function(){Va()}});e.STATIC_BASE=ya;e.STATIC_BUMP=59888;x+=16;
var H={G:1,u:2,zc:3,vb:4,B:5,ga:6,Oa:7,Tb:8,s:9,bb:10,ba:11,Jc:11,wa:12,R:13,ob:14,ec:15,S:16,da:17,Kc:18,U:19,ea:20,J:21,h:22,Ob:23,va:24,C:25,Gc:26,pb:27,ac:28,N:29,wc:30,Hb:31,pc:32,lb:33,tc:34,Xb:42,sb:43,cb:44,yb:45,zb:46,Ab:47,Gb:48,Hc:49,Rb:50,xb:51,ib:35,Ub:37,Ua:52,Xa:53,Lc:54,Pb:55,Ya:56,Za:57,jb:35,$a:59,cc:60,Sb:61,Dc:62,bc:63,Yb:64,Zb:65,vc:66,Vb:67,Ra:68,Ac:69,eb:70,qc:71,Jb:72,mb:73,Wa:74,kc:76,Va:77,uc:78,Bb:79,Cb:80,Fb:81,Eb:82,Db:83,dc:38,fa:39,Kb:36,T:40,lc:95,oc:96,hb:104,Qb:105,
Sa:97,sc:91,ic:88,$b:92,xc:108,gb:111,Pa:98,fb:103,Nb:101,Lb:100,Ec:110,qb:112,rb:113,ub:115,Ta:114,kb:89,Ib:90,rc:93,yc:94,Qa:99,Mb:102,wb:106,fc:107,Fc:109,Ic:87,nb:122,Bc:116,jc:95,Wb:123,tb:84,mc:75,ab:125,hc:131,nc:130,Cc:86},Wa={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",
13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",
35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",
54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",
75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",
92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",
109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};function Xa(a){e.___errno_location&&(D[e.___errno_location()>>2]=a);return a}
function Ya(a,b){for(var c=0,d=a.length-1;0<=d;d--){var f=a[d];"."===f?a.splice(d,1):".."===f?(a.splice(d,1),c++):c&&(a.splice(d,1),c--)}if(b)for(;c;c--)a.unshift("..");return a}function Za(a){var b="/"===a.charAt(0),c="/"===a.substr(-1);(a=Ya(a.split("/").filter(function(a){return!!a}),!b).join("/"))||b||(a=".");a&&c&&(a+="/");return(b?"/":"")+a}
function $a(a){var b=/^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/.exec(a).slice(1);a=b[0];b=b[1];if(!a&&!b)return".";b&&(b=b.substr(0,b.length-1));return a+b}function ab(a){if("/"===a)return"/";var b=a.lastIndexOf("/");return-1===b?a:a.substr(b+1)}function bb(){var a=Array.prototype.slice.call(arguments,0);return Za(a.join("/"))}function I(a,b){return Za(a+"/"+b)}
function cb(){for(var a="",b=!1,c=arguments.length-1;-1<=c&&!b;c--){b=0<=c?arguments[c]:"/";if("string"!==typeof b)throw new TypeError("Arguments to path.resolve must be strings");if(!b)return"";a=b+"/"+a;b="/"===b.charAt(0)}a=Ya(a.split("/").filter(function(a){return!!a}),!b).join("/");return(b?"/":"")+a||"."}var db=[];function eb(a,b){db[a]={input:[],output:[],I:b};fb(a,gb)}
var gb={open:function(a){var b=db[a.node.rdev];if(!b)throw new K(H.U);a.tty=b;a.seekable=!1},close:function(a){a.tty.I.flush(a.tty)},flush:function(a){a.tty.I.flush(a.tty)},read:function(a,b,c,d){if(!a.tty||!a.tty.I.pa)throw new K(H.ga);for(var f=0,g=0;g<d;g++){try{var h=a.tty.I.pa(a.tty)}catch(m){throw new K(H.B);}if(void 0===h&&0===f)throw new K(H.ba);if(null===h||void 0===h)break;f++;b[c+g]=h}f&&(a.node.timestamp=Date.now());return f},write:function(a,b,c,d){if(!a.tty||!a.tty.I.$)throw new K(H.ga);
for(var f=0;f<d;f++)try{a.tty.I.$(a.tty,b[c+f])}catch(g){throw new K(H.B);}d&&(a.node.timestamp=Date.now());return f}},ib={pa:function(a){if(!a.input.length){var b=null;if(r){var c=new Buffer(256),d=0,f=process.stdin.fd;if("win32"!=process.platform){var g=!1;try{f=fs.openSync("/dev/stdin","r"),g=!0}catch(h){}}try{d=fs.readSync(f,c,0,256,null)}catch(h){if(-1!=h.toString().indexOf("EOF"))d=0;else throw h;}g&&fs.closeSync(f);0<d?b=c.slice(0,d).toString("utf-8"):b=null}else"undefined"!=typeof window&&
"function"==typeof window.prompt?(b=window.prompt("Input: "),null!==b&&(b+="\n")):"function"==typeof readline&&(b=readline(),null!==b&&(b+="\n"));if(!b)return null;a.input=hb(b)}return a.input.shift()},$:function(a,b){null===b||10===b?(fa(C(a.output,0)),a.output=[]):0!=b&&a.output.push(b)},flush:function(a){a.output&&0<a.output.length&&(fa(C(a.output,0)),a.output=[])}},jb={$:function(a,b){null===b||10===b?(w(C(a.output,0)),a.output=[]):0!=b&&a.output.push(b)},flush:function(a){a.output&&0<a.output.length&&
(w(C(a.output,0)),a.output=[])}},L={m:null,j:function(){return L.createNode(null,"/",16895,0)},createNode:function(a,b,c,d){if(24576===(c&61440)||4096===(c&61440))throw new K(H.G);L.m||(L.m={dir:{node:{o:L.c.o,i:L.c.i,lookup:L.c.lookup,K:L.c.K,rename:L.c.rename,unlink:L.c.unlink,rmdir:L.c.rmdir,readdir:L.c.readdir,symlink:L.c.symlink},stream:{A:L.f.A}},file:{node:{o:L.c.o,i:L.c.i},stream:{A:L.f.A,read:L.f.read,write:L.f.write,ha:L.f.ha,sa:L.f.sa,ua:L.f.ua}},link:{node:{o:L.c.o,i:L.c.i,readlink:L.c.readlink},
stream:{}},ka:{node:{o:L.c.o,i:L.c.i},stream:kb}});c=lb(a,b,c,d);M(c.mode)?(c.c=L.m.dir.node,c.f=L.m.dir.stream,c.b={}):32768===(c.mode&61440)?(c.c=L.m.file.node,c.f=L.m.file.stream,c.g=0,c.b=null):40960===(c.mode&61440)?(c.c=L.m.link.node,c.f=L.m.link.stream):8192===(c.mode&61440)&&(c.c=L.m.ka.node,c.f=L.m.ka.stream);c.timestamp=Date.now();a&&(a.b[b]=c);return c},Ea:function(a){if(a.b&&a.b.subarray){for(var b=[],c=0;c<a.g;++c)b.push(a.b[c]);return b}return a.b},Oc:function(a){return a.b?a.b.subarray?
a.b.subarray(0,a.g):new Uint8Array(a.b):new Uint8Array},la:function(a,b){a.b&&a.b.subarray&&b>a.b.length&&(a.b=L.Ea(a),a.g=a.b.length);if(!a.b||a.b.subarray){var c=a.b?a.b.length:0;c>=b||(b=Math.max(b,c*(1048576>c?2:1.125)|0),0!=c&&(b=Math.max(b,256)),c=a.b,a.b=new Uint8Array(b),0<a.g&&a.b.set(c.subarray(0,a.g),0))}else for(!a.b&&0<b&&(a.b=[]);a.b.length<b;)a.b.push(0)},Ja:function(a,b){if(a.g!=b)if(0==b)a.b=null,a.g=0;else{if(!a.b||a.b.subarray){var c=a.b;a.b=new Uint8Array(new ArrayBuffer(b));c&&
a.b.set(c.subarray(0,Math.min(b,a.g)))}else if(a.b||(a.b=[]),a.b.length>b)a.b.length=b;else for(;a.b.length<b;)a.b.push(0);a.g=b}},c:{o:function(a){var b={};b.dev=8192===(a.mode&61440)?a.id:1;b.ino=a.id;b.mode=a.mode;b.nlink=1;b.uid=0;b.gid=0;b.rdev=a.rdev;M(a.mode)?b.size=4096:32768===(a.mode&61440)?b.size=a.g:40960===(a.mode&61440)?b.size=a.link.length:b.size=0;b.atime=new Date(a.timestamp);b.mtime=new Date(a.timestamp);b.ctime=new Date(a.timestamp);b.F=4096;b.blocks=Math.ceil(b.size/b.F);return b},
i:function(a,b){void 0!==b.mode&&(a.mode=b.mode);void 0!==b.timestamp&&(a.timestamp=b.timestamp);void 0!==b.size&&L.Ja(a,b.size)},lookup:function(){throw mb[H.u];},K:function(a,b,c,d){return L.createNode(a,b,c,d)},rename:function(a,b,c){if(M(a.mode)){try{var d=N(b,c)}catch(g){}if(d)for(var f in d.b)throw new K(H.fa);}delete a.parent.b[a.name];a.name=c;b.b[c]=a;a.parent=b},unlink:function(a,b){delete a.b[b]},rmdir:function(a,b){var c=N(a,b),d;for(d in c.b)throw new K(H.fa);delete a.b[b]},readdir:function(a){var b=
[".",".."],c;for(c in a.b)a.b.hasOwnProperty(c)&&b.push(c);return b},symlink:function(a,b,c){a=L.createNode(a,b,41471,0);a.link=c;return a},readlink:function(a){if(40960!==(a.mode&61440))throw new K(H.h);return a.link}},f:{read:function(a,b,c,d,f){var g=a.node.b;if(f>=a.node.g)return 0;a=Math.min(a.node.g-f,d);assert(0<=a);if(8<a&&g.subarray)b.set(g.subarray(f,f+a),c);else for(d=0;d<a;d++)b[c+d]=g[f+d];return a},write:function(a,b,c,d,f,g){if(!d)return 0;a=a.node;a.timestamp=Date.now();if(b.subarray&&
(!a.b||a.b.subarray)){if(g)return a.b=b.subarray(c,c+d),a.g=d;if(0===a.g&&0===f)return a.b=new Uint8Array(b.subarray(c,c+d)),a.g=d;if(f+d<=a.g)return a.b.set(b.subarray(c,c+d),f),d}L.la(a,f+d);if(a.b.subarray&&b.subarray)a.b.set(b.subarray(c,c+d),f);else for(g=0;g<d;g++)a.b[f+g]=b[c+g];a.g=Math.max(a.g,f+d);return d},A:function(a,b,c){1===c?b+=a.position:2===c&&32768===(a.node.mode&61440)&&(b+=a.node.g);if(0>b)throw new K(H.h);return b},ha:function(a,b,c){L.la(a.node,b+c);a.node.g=Math.max(a.node.g,
b+c)},sa:function(a,b,c,d,f,g,h){if(32768!==(a.node.mode&61440))throw new K(H.U);c=a.node.b;if(h&2||c.buffer!==b&&c.buffer!==b.buffer){if(0<f||f+d<a.node.g)c.subarray?c=c.subarray(f,f+d):c=Array.prototype.slice.call(c,f,f+d);a=!0;d=nb(d);if(!d)throw new K(H.wa);b.set(c,d)}else a=!1,d=c.byteOffset;return{Qc:d,Mc:a}},ua:function(a,b,c,d,f){if(32768!==(a.node.mode&61440))throw new K(H.U);if(f&2)return 0;L.f.write(a,b,0,d,c,!1);return 0}}},O={P:!1,Ma:function(){O.P=!!process.platform.match(/^win/);var a=
process.binding("constants");a.fs&&(a=a.fs);O.ma={1024:a.O_APPEND,64:a.O_CREAT,128:a.O_EXCL,0:a.O_RDONLY,2:a.O_RDWR,4096:a.O_SYNC,512:a.O_TRUNC,1:a.O_WRONLY}},ia:function(a){return Buffer.D?Buffer.from(a):new Buffer(a)},j:function(a){assert(r);return O.createNode(null,"/",O.oa(a.Z.root),0)},createNode:function(a,b,c){if(!M(c)&&32768!==(c&61440)&&40960!==(c&61440))throw new K(H.h);a=lb(a,b,c);a.c=O.c;a.f=O.f;return a},oa:function(a){try{var b=fs.lstatSync(a);O.P&&(b.mode=b.mode|(b.mode&292)>>2)}catch(c){if(!c.code)throw c;
throw new K(H[c.code]);}return b.mode},l:function(a){for(var b=[];a.parent!==a;)b.push(a.name),a=a.parent;b.push(a.j.Z.root);b.reverse();return bb.apply(null,b)},Ca:function(a){a&=-2656257;var b=0,c;for(c in O.ma)a&c&&(b|=O.ma[c],a^=c);if(a)throw new K(H.h);return b},c:{o:function(a){a=O.l(a);try{var b=fs.lstatSync(a)}catch(c){if(!c.code)throw c;throw new K(H[c.code]);}O.P&&!b.F&&(b.F=4096);O.P&&!b.blocks&&(b.blocks=(b.size+b.F-1)/b.F|0);return{dev:b.dev,ino:b.ino,mode:b.mode,nlink:b.nlink,uid:b.uid,
gid:b.gid,rdev:b.rdev,size:b.size,atime:b.atime,mtime:b.mtime,ctime:b.ctime,F:b.F,blocks:b.blocks}},i:function(a,b){var c=O.l(a);try{void 0!==b.mode&&(fs.chmodSync(c,b.mode),a.mode=b.mode),void 0!==b.size&&fs.truncateSync(c,b.size)}catch(d){if(!d.code)throw d;throw new K(H[d.code]);}},lookup:function(a,b){var c=I(O.l(a),b);c=O.oa(c);return O.createNode(a,b,c)},K:function(a,b,c,d){a=O.createNode(a,b,c,d);b=O.l(a);try{M(a.mode)?fs.mkdirSync(b,a.mode):fs.writeFileSync(b,"",{mode:a.mode})}catch(f){if(!f.code)throw f;
throw new K(H[f.code]);}return a},rename:function(a,b,c){a=O.l(a);b=I(O.l(b),c);try{fs.renameSync(a,b)}catch(d){if(!d.code)throw d;throw new K(H[d.code]);}},unlink:function(a,b){a=I(O.l(a),b);try{fs.unlinkSync(a)}catch(c){if(!c.code)throw c;throw new K(H[c.code]);}},rmdir:function(a,b){a=I(O.l(a),b);try{fs.rmdirSync(a)}catch(c){if(!c.code)throw c;throw new K(H[c.code]);}},readdir:function(a){a=O.l(a);try{return fs.readdirSync(a)}catch(b){if(!b.code)throw b;throw new K(H[b.code]);}},symlink:function(a,
b,c){a=I(O.l(a),b);try{fs.symlinkSync(c,a)}catch(d){if(!d.code)throw d;throw new K(H[d.code]);}},readlink:function(a){var b=O.l(a);try{return b=fs.readlinkSync(b),b=ob.relative(ob.resolve(a.j.Z.root),b)}catch(c){if(!c.code)throw c;throw new K(H[c.code]);}}},f:{open:function(a){var b=O.l(a.node);try{32768===(a.node.mode&61440)&&(a.M=fs.openSync(b,O.Ca(a.flags)))}catch(c){if(!c.code)throw c;throw new K(H[c.code]);}},close:function(a){try{32768===(a.node.mode&61440)&&a.M&&fs.closeSync(a.M)}catch(b){if(!b.code)throw b;
throw new K(H[b.code]);}},read:function(a,b,c,d,f){if(0===d)return 0;try{return fs.readSync(a.M,O.ia(b.buffer),c,d,f)}catch(g){throw new K(H[g.code]);}},write:function(a,b,c,d,f){try{return fs.writeSync(a.M,O.ia(b.buffer),c,d,f)}catch(g){throw new K(H[g.code]);}},A:function(a,b,c){if(1===c)b+=a.position;else if(2===c&&32768===(a.node.mode&61440))try{b+=fs.fstatSync(a.M).size}catch(d){throw new K(H[d.code]);}if(0>b)throw new K(H.h);return b}}};x+=16;x+=16;x+=16;
var pb=null,qb={},P=[],rb=1,Q=null,sb=!0,R={},K=null,mb={};
function S(a,b){a=cb("/",a);b=b||{};if(!a)return{path:"",node:null};var c={na:!0,aa:0},d;for(d in c)void 0===b[d]&&(b[d]=c[d]);if(8<b.aa)throw new K(H.T);a=Ya(a.split("/").filter(function(a){return!!a}),!1);var f=pb;c="/";for(d=0;d<a.length;d++){var g=d===a.length-1;if(g&&b.parent)break;f=N(f,a[d]);c=I(c,a[d]);f.L&&(!g||g&&b.na)&&(f=f.L.root);if(!g||b.W)for(g=0;40960===(f.mode&61440);)if(f=tb(c),c=cb($a(c),f),f=S(c,{aa:b.aa}).node,40<g++)throw new K(H.T);}return{path:c,node:f}}
function T(a){for(var b;;){if(a===a.parent)return a=a.j.ta,b?"/"!==a[a.length-1]?a+"/"+b:a+b:a;b=b?a.name+"/"+b:a.name;a=a.parent}}function ub(a,b){for(var c=0,d=0;d<b.length;d++)c=(c<<5)-c+b.charCodeAt(d)|0;return(a+c>>>0)%Q.length}function vb(a){var b=ub(a.parent.id,a.name);a.H=Q[b];Q[b]=a}function N(a,b){var c;if(c=(c=wb(a,"x"))?c:a.c.lookup?0:H.R)throw new K(c,a);for(c=Q[ub(a.id,b)];c;c=c.H){var d=c.name;if(c.parent.id===a.id&&d===b)return c}return a.c.lookup(a,b)}
function lb(a,b,c,d){xb||(xb=function(a,b,c,d){a||(a=this);this.parent=a;this.j=a.j;this.L=null;this.id=rb++;this.name=b;this.mode=c;this.c={};this.f={};this.rdev=d},xb.prototype={},Object.defineProperties(xb.prototype,{read:{get:function(){return 365===(this.mode&365)},set:function(a){a?this.mode|=365:this.mode&=-366}},write:{get:function(){return 146===(this.mode&146)},set:function(a){a?this.mode|=146:this.mode&=-147}},Ha:{get:function(){return M(this.mode)}},Ga:{get:function(){return 8192===(this.mode&
61440)}}}));a=new xb(a,b,c,d);vb(a);return a}function M(a){return 16384===(a&61440)}var yb={r:0,rs:1052672,"r+":2,w:577,wx:705,xw:705,"w+":578,"wx+":706,"xw+":706,a:1089,ax:1217,xa:1217,"a+":1090,"ax+":1218,"xa+":1218};function zb(a){var b=["r","w","rw"][a&3];a&512&&(b+="w");return b}function wb(a,b){if(sb)return 0;if(-1===b.indexOf("r")||a.mode&292){if(-1!==b.indexOf("w")&&!(a.mode&146)||-1!==b.indexOf("x")&&!(a.mode&73))return H.R}else return H.R;return 0}
function Ab(a,b){try{return N(a,b),H.da}catch(c){}return wb(a,"wx")}function Bb(a){var b=4096;for(a=a||0;a<=b;a++)if(!P[a])return a;throw new K(H.va);}function Cb(a,b){Db||(Db=function(){},Db.prototype={},Object.defineProperties(Db.prototype,{object:{get:function(){return this.node},set:function(a){this.node=a}}}));var c=new Db,d;for(d in a)c[d]=a[d];a=c;b=Bb(b);a.fd=b;return P[b]=a}var kb={open:function(a){a.f=qb[a.node.rdev].f;a.f.open&&a.f.open(a)},A:function(){throw new K(H.N);}};
function fb(a,b){qb[a]={f:b}}function Eb(a,b){var c="/"===b,d=!b;if(c&&pb)throw new K(H.S);if(!c&&!d){var f=S(b,{na:!1});b=f.path;f=f.node;if(f.L)throw new K(H.S);if(!M(f.mode))throw new K(H.ea);}b={type:a,Z:{},ta:b,Ia:[]};a=a.j(b);a.j=b;b.root=a;c?pb=a:f&&(f.L=b,f.j&&f.j.Ia.push(b))}function Fb(a,b,c){var d=S(a,{parent:!0}).node;a=ab(a);if(!a||"."===a||".."===a)throw new K(H.h);var f=Ab(d,a);if(f)throw new K(f);if(!d.c.K)throw new K(H.G);return d.c.K(d,a,b,c)}
function U(a,b){return Fb(a,(void 0!==b?b:511)&1023|16384,0)}function Gb(a,b,c){"undefined"===typeof c&&(c=b,b=438);return Fb(a,b|8192,c)}function Hb(a,b){if(!cb(a))throw new K(H.u);var c=S(b,{parent:!0}).node;if(!c)throw new K(H.u);b=ab(b);var d=Ab(c,b);if(d)throw new K(d);if(!c.c.symlink)throw new K(H.G);return c.c.symlink(c,b,a)}
function Ib(a){var b=S(a,{parent:!0}).node,c=ab(a),d=N(b,c);a:{try{var f=N(b,c)}catch(h){f=h.v;break a}var g=wb(b,"wx");f=g?g:M(f.mode)?H.J:0}if(f)throw new K(f);if(!b.c.unlink)throw new K(H.G);if(d.L)throw new K(H.S);try{R.willDeletePath&&R.willDeletePath(a)}catch(h){console.log("FS.trackingDelegate['willDeletePath']('"+a+"') threw an exception: "+h.message)}b.c.unlink(b,c);b=ub(d.parent.id,d.name);if(Q[b]===d)Q[b]=d.H;else for(b=Q[b];b;){if(b.H===d){b.H=d.H;break}b=b.H}try{if(R.onDeletePath)R.onDeletePath(a)}catch(h){console.log("FS.trackingDelegate['onDeletePath']('"+
a+"') threw an exception: "+h.message)}}function tb(a){a=S(a).node;if(!a)throw new K(H.u);if(!a.c.readlink)throw new K(H.h);return cb(T(a.parent),a.c.readlink(a))}function Jb(a,b){var c;"string"===typeof a?c=S(a,{W:!0}).node:c=a;if(!c.c.i)throw new K(H.G);c.c.i(c,{mode:b&4095|c.mode&-4096,timestamp:Date.now()})}
function V(a,b,c,d){if(""===a)throw new K(H.u);if("string"===typeof b){var f=yb[b];if("undefined"===typeof f)throw Error("Unknown file open mode: "+b);b=f}c=b&64?("undefined"===typeof c?438:c)&4095|32768:0;if("object"===typeof a)var g=a;else{a=Za(a);try{g=S(a,{W:!(b&131072)}).node}catch(p){}}f=!1;if(b&64)if(g){if(b&128)throw new K(H.da);}else g=Fb(a,c,0),f=!0;if(!g)throw new K(H.u);8192===(g.mode&61440)&&(b&=-513);if(b&65536&&!M(g.mode))throw new K(H.ea);if(!f){var h=g?40960===(g.mode&61440)?H.T:
M(g.mode)&&("r"!==zb(b)||b&512)?H.J:wb(g,zb(b)):H.u;if(h)throw new K(h);}if(b&512){c=g;var m;"string"===typeof c?m=S(c,{W:!0}).node:m=c;if(!m.c.i)throw new K(H.G);if(M(m.mode))throw new K(H.J);if(32768!==(m.mode&61440))throw new K(H.h);if(c=wb(m,"w"))throw new K(c);m.c.i(m,{size:0,timestamp:Date.now()})}b&=-641;d=Cb({node:g,path:T(g),flags:b,seekable:!0,position:0,f:g.f,Na:[],error:!1},d);d.f.open&&d.f.open(d);!e.logReadFiles||b&1||(Kb||(Kb={}),a in Kb||(Kb[a]=1,h("read file: "+a)));try{R.onOpenFile&&
(h=0,1!==(b&2097155)&&(h|=1),0!==(b&2097155)&&(h|=2),R.onOpenFile(a,h))}catch(p){console.log("FS.trackingDelegate['onOpenFile']('"+a+"', flags) threw an exception: "+p.message)}return d}function Lb(a){if(null===a.fd)throw new K(H.s);a.X&&(a.X=null);try{a.f.close&&a.f.close(a)}catch(b){throw b;}finally{P[a.fd]=null}a.fd=null}function Mb(a,b,c){if(null===a.fd)throw new K(H.s);if(!a.seekable||!a.f.A)throw new K(H.N);a.position=a.f.A(a,b,c);a.Na=[]}
function Nb(a,b,c,d,f,g){if(0>d||0>f)throw new K(H.h);if(null===a.fd)throw new K(H.s);if(0===(a.flags&2097155))throw new K(H.s);if(M(a.node.mode))throw new K(H.J);if(!a.f.write)throw new K(H.h);a.flags&1024&&Mb(a,0,2);var h="undefined"!==typeof f;if(!h)f=a.position;else if(!a.seekable)throw new K(H.N);b=a.f.write(a,b,c,d,f,g);h||(a.position+=b);try{if(a.path&&R.onWriteToFile)R.onWriteToFile(a.path)}catch(m){console.log("FS.trackingDelegate['onWriteToFile']('"+path+"') threw an exception: "+m.message)}return b}
function Ob(){K||(K=function(a,b){this.node=b;this.La=function(a){this.v=a;for(var b in H)if(H[b]===a){this.code=b;break}};this.La(a);this.message=Wa[a];this.stack&&Object.defineProperty(this,"stack",{value:Error().stack,writable:!0})},K.prototype=Error(),K.prototype.constructor=K,[H.u].forEach(function(a){mb[a]=new K(a);mb[a].stack="<generic error, no stack>"}))}var Pb;function Qb(a,b){var c=0;a&&(c|=365);b&&(c|=146);return c}
function Rb(a,b,c,d){a=I("string"===typeof a?a:T(a),b);return U(a,Qb(c,d))}function Sb(a,b){a="string"===typeof a?a:T(a);for(b=b.split("/").reverse();b.length;){var c=b.pop();if(c){var d=I(a,c);try{U(d)}catch(f){}a=d}}return d}function Tb(a,b,c,d){a=I("string"===typeof a?a:T(a),b);c=Qb(c,d);return Fb(a,(void 0!==c?c:438)&4095|32768,0)}
function Ub(a,b,c,d,f,g){a=b?I("string"===typeof a?a:T(a),b):a;d=Qb(d,f);f=Fb(a,(void 0!==d?d:438)&4095|32768,0);if(c){if("string"===typeof c){a=Array(c.length);b=0;for(var h=c.length;b<h;++b)a[b]=c.charCodeAt(b);c=a}Jb(f,d|146);a=V(f,"w");Nb(a,c,0,c.length,0,g);Lb(a);Jb(f,d)}return f}
function W(a,b,c,d){a=I("string"===typeof a?a:T(a),b);b=Qb(!!c,!!d);W.ra||(W.ra=64);var f=W.ra++<<8|0;fb(f,{open:function(a){a.seekable=!1},close:function(){d&&d.buffer&&d.buffer.length&&d(10)},read:function(a,b,d,f){for(var g=0,h=0;h<f;h++){try{var m=c()}catch(Na){throw new K(H.B);}if(void 0===m&&0===g)throw new K(H.ba);if(null===m||void 0===m)break;g++;b[d+h]=m}g&&(a.node.timestamp=Date.now());return g},write:function(a,b,c,f){for(var g=0;g<f;g++)try{d(b[c+g])}catch(B){throw new K(H.B);}f&&(a.node.timestamp=
Date.now());return g}});return Gb(a,b,f)}function Vb(a,b,c){a=I("string"===typeof a?a:T(a),b);return Hb(c,a)}
function Wb(a){if(a.Ga||a.Ha||a.link||a.b)return!0;var b=!0;if("undefined"!==typeof XMLHttpRequest)throw Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");if(e.read)try{a.b=hb(e.read(a.url)),a.g=a.b.length}catch(c){b=!1}else throw Error("Cannot load without read() or XMLHttpRequest.");b||Xa(H.B);return b}
function Xb(a,b,c,d,f){function g(){this.Y=!1;this.O=[]}g.prototype.get=function(a){if(!(a>this.length-1||0>a)){var b=a%this.chunkSize;return this.qa(a/this.chunkSize|0)[b]}};g.prototype.Ka=function(a){this.qa=a};g.prototype.ja=function(){var a=new XMLHttpRequest;a.open("HEAD",c,!1);a.send(null);if(!(200<=a.status&&300>a.status||304===a.status))throw Error("Couldn't load "+c+". Status: "+a.status);var b=Number(a.getResponseHeader("Content-length")),d,f=(d=a.getResponseHeader("Accept-Ranges"))&&"bytes"===
d;a=(d=a.getResponseHeader("Content-Encoding"))&&"gzip"===d;var g=1048576;f||(g=b);var h=this;h.Ka(function(a){var d=a*g,f=(a+1)*g-1;f=Math.min(f,b-1);if("undefined"===typeof h.O[a]){var m=h.O;if(d>f)throw Error("invalid range ("+d+", "+f+") or no bytes requested!");if(f>b-1)throw Error("only "+b+" bytes available! programmer error!");var p=new XMLHttpRequest;p.open("GET",c,!1);b!==g&&p.setRequestHeader("Range","bytes="+d+"-"+f);"undefined"!=typeof Uint8Array&&(p.responseType="arraybuffer");p.overrideMimeType&&
p.overrideMimeType("text/plain; charset=x-user-defined");p.send(null);if(!(200<=p.status&&300>p.status||304===p.status))throw Error("Couldn't load "+c+". Status: "+p.status);d=void 0!==p.response?new Uint8Array(p.response||[]):hb(p.responseText||"");m[a]=d}if("undefined"===typeof h.O[a])throw Error("doXHR failed!");return h.O[a]});if(a||!b)g=b=1,g=b=this.qa(0).length,console.log("LazyFiles on gzip forces download of the whole file when length is accessed");this.za=b;this.ya=g;this.Y=!0};if("undefined"!==
typeof XMLHttpRequest){if(!q)throw"Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc";var h=new g;Object.defineProperties(h,{length:{get:function(){this.Y||this.ja();return this.za}},chunkSize:{get:function(){this.Y||this.ja();return this.ya}}});var m=void 0}else m=c,h=void 0;var p=Tb(a,b,d,f);h?p.b=h:m&&(p.b=null,p.url=m);Object.defineProperties(p,{g:{get:function(){return this.b.length}}});var v={};Object.keys(p.f).forEach(function(a){var b=
p.f[a];v[a]=function(){if(!Wb(p))throw new K(H.B);return b.apply(null,arguments)}});v.read=function(a,b,c,d,f){if(!Wb(p))throw new K(H.B);a=a.node.b;if(f>=a.length)return 0;d=Math.min(a.length-f,d);assert(0<=d);if(a.slice)for(var g=0;g<d;g++)b[c+g]=a[f+g];else for(g=0;g<d;g++)b[c+g]=a.get(f+g);return d};p.f=v;return p}
function Yb(a,b,c,d,f,g,h,m,p,v){function B(c){function B(c){v&&v();m||Ub(a,b,c,d,f,p);g&&g();Ta()}var J=!1;e.preloadPlugins.forEach(function(a){!J&&a.canHandle(ba)&&(a.handle(c,ba,B,function(){h&&h();Ta()}),J=!0)});J||B(c)}Browser.Pc();var ba=b?cb(I(a,b)):a;Sa();"string"==typeof c?Browser.Nc(c,function(a){B(a)},h):B(c)}var FS={},xb,Db,Kb,X=0;function Y(){X+=4;return D[X-4>>2]}function Z(){var a=P[Y()];if(!a)throw new K(H.s);return a}function Zb(a){return Math.pow(2,a)}var $b={},ac=1;
function bc(a,b){bc.D||(bc.D={});a in bc.D||(e.dynCall_v(b),bc.D[a]=1)}Ob();Q=Array(4096);Eb(L,"/");U("/tmp");U("/home");U("/home/web_user");
(function(){U("/dev");fb(259,{read:function(){return 0},write:function(a,b,f,g){return g}});Gb("/dev/null",259);eb(1280,ib);eb(1536,jb);Gb("/dev/tty",1280);Gb("/dev/tty1",1536);if("undefined"!==typeof crypto){var a=new Uint8Array(1);var b=function(){crypto.getRandomValues(a);return a[0]}}else r?b=function(){return require("crypto").randomBytes(1)[0]}:b=function(){u("random_device")};W("/dev","random",b);W("/dev","urandom",b);U("/dev/shm");U("/dev/shm/tmp")})();U("/proc");U("/proc/self");U("/proc/self/fd");
Eb({j:function(){var a=lb("/proc/self","fd",16895,73);a.c={lookup:function(a,c){var b=P[+c];if(!b)throw new K(H.s);a={parent:null,j:{ta:"fake"},c:{readlink:function(){return b.path}}};return a.parent=a}};return a}},"/proc/self/fd");
Ia.unshift(function(){if(!e.noFSInit&&!Pb){assert(!Pb,"FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)");Pb=!0;Ob();e.stdin=e.stdin;e.stdout=e.stdout;e.stderr=e.stderr;e.stdin?W("/dev","stdin",e.stdin):Hb("/dev/tty","/dev/stdin");e.stdout?W("/dev","stdout",null,e.stdout):Hb("/dev/tty","/dev/stdout");e.stderr?W("/dev","stderr",null,e.stderr):Hb("/dev/tty1","/dev/stderr");var a=
V("/dev/stdin","r");assert(0===a.fd,"invalid handle for stdin ("+a.fd+")");a=V("/dev/stdout","w");assert(1===a.fd,"invalid handle for stdout ("+a.fd+")");a=V("/dev/stderr","w");assert(2===a.fd,"invalid handle for stderr ("+a.fd+")")}});Ja.push(function(){sb=!1});Ka.push(function(){Pb=!1;var a=e._fflush;a&&a(0);for(a=0;a<P.length;a++){var b=P[a];b&&Lb(b)}});e.FS_createFolder=Rb;e.FS_createPath=Sb;e.FS_createDataFile=Ub;e.FS_createPreloadedFile=Yb;e.FS_createLazyFile=Xb;e.FS_createLink=Vb;
e.FS_createDevice=W;e.FS_unlink=Ib;Ia.unshift(function(){});Ka.push(function(){});if(r){var fs=require("fs"),ob=require("path");O.Ma()}E=ha(4);Aa=Ba=ia(x);Ca=Aa+Fa;Da=ia(Ca);D[E>>2]=Da;za=!0;function hb(a){for(var b=0,c=0;c<a.length;++c){var d=a.charCodeAt(c);55296<=d&&57343>=d&&(d=65536+((d&1023)<<10)|a.charCodeAt(++c)&1023);127>=d?++b:b=2047>=d?b+2:65535>=d?b+3:2097151>=d?b+4:67108863>=d?b+5:b+6}b=Array(b+1);a=oa(a,b,0,b.length);b.length=a;return b}e.wasmTableSize=1052;e.wasmMaxTableSize=1052;
e.Aa={};
e.Ba={abort:u,enlargeMemory:function(){Ea()},getTotalMemory:function(){return F},abortOnCannotGrowMemory:Ea,___assert_fail:function(a,b,c,d){u("Assertion failed: "+A(a)+", at: "+[b?A(b):"unknown filename",c,d?A(d):"unknown function"])},___cxa_allocate_exception:function(a){return nb(a)},___cxa_pure_virtual:function(){ka=!0;throw"Pure virtual function called!";},___cxa_throw:function(a){"uncaught_exception"in cc?cc.D++:cc.D=1;throw a+" - Exception catching is disabled, this exception cannot be caught. Compile with -s DISABLE_EXCEPTION_CATCHING=0 or DISABLE_EXCEPTION_CATCHING=2 to catch.";},
___cxa_uncaught_exception:function(){return!!cc.D},___lock:function(){},___setErrNo:Xa,___syscall140:function(a,b){X=b;try{var c=Z();Y();var d=Y(),f=Y(),g=Y();Mb(c,d,g);D[f>>2]=c.position;c.X&&0===d&&0===g&&(c.X=null);return 0}catch(h){return"undefined"!==typeof FS&&h instanceof K||u(h),-h.v}},___syscall145:function(a,b){X=b;try{var c=Z(),d=Y();a:{var f=Y();for(b=a=0;b<f;b++){var g=D[d+(8*b+4)>>2],h=c,m=D[d+8*b>>2],p=g,v=void 0,B=y;if(0>p||0>v)throw new K(H.h);if(null===h.fd)throw new K(H.s);if(1===
(h.flags&2097155))throw new K(H.s);if(M(h.node.mode))throw new K(H.J);if(!h.f.read)throw new K(H.h);var ba="undefined"!==typeof v;if(!ba)v=h.position;else if(!h.seekable)throw new K(H.N);var Na=h.f.read(h,B,m,p,v);ba||(h.position+=Na);var pa=Na;if(0>pa){var J=-1;break a}a+=pa;if(pa<g)break}J=a}return J}catch(Ma){return"undefined"!==typeof FS&&Ma instanceof K||u(Ma),-Ma.v}},___syscall146:function(a,b){X=b;try{var c=Z(),d=Y();a:{var f=Y();for(b=a=0;b<f;b++){var g=Nb(c,y,D[d+8*b>>2],D[d+(8*b+4)>>2],
void 0);if(0>g){var h=-1;break a}a+=g}h=a}return h}catch(m){return"undefined"!==typeof FS&&m instanceof K||u(m),-m.v}},___syscall221:function(a,b){X=b;try{var c=Z();switch(Y()){case 0:var d=Y();return 0>d?-H.h:V(c.path,c.flags,0,d).fd;case 1:case 2:return 0;case 3:return c.flags;case 4:return d=Y(),c.flags|=d,0;case 12:case 12:return d=Y(),ua[d+0>>1]=2,0;case 13:case 14:case 13:case 14:return 0;case 16:case 8:return-H.h;case 9:return Xa(H.h),-1;default:return-H.h}}catch(f){return"undefined"!==typeof FS&&
f instanceof K||u(f),-f.v}},___syscall5:function(a,b){X=b;try{var c=A(Y()),d=Y(),f=Y();return V(c,d,f).fd}catch(g){return"undefined"!==typeof FS&&g instanceof K||u(g),-g.v}},___syscall54:function(a,b){X=b;try{var c=Z(),d=Y();switch(d){case 21509:case 21505:return c.tty?0:-H.C;case 21510:case 21511:case 21512:case 21506:case 21507:case 21508:return c.tty?0:-H.C;case 21519:if(!c.tty)return-H.C;var f=Y();return D[f>>2]=0;case 21520:return c.tty?-H.h:-H.C;case 21531:a=f=Y();if(!c.f.Fa)throw new K(H.C);
return c.f.Fa(c,d,a);case 21523:return c.tty?0:-H.C;case 21524:return c.tty?0:-H.C;default:u("bad ioctl syscall "+d)}}catch(g){return"undefined"!==typeof FS&&g instanceof K||u(g),-g.v}},___syscall6:function(a,b){X=b;try{var c=Z();Lb(c);return 0}catch(d){return"undefined"!==typeof FS&&d instanceof K||u(d),-d.v}},___unlock:function(){},_abort:function(){e.abort()},_ems_request_file:function(a){var b=window.ScriptNodePlayer.getInstance();if(b.isReady())return b._fileRequestCallback(a);window.console.log("error: ems_request_file not ready")},
_emscripten_memcpy_big:function(a,b,c){z.set(z.subarray(b,b+c),a);return a},_llvm_exp2_f64:function(){return Zb.apply(null,arguments)},_llvm_trap:function(){u("trap!")},_pthread_cond_wait:function(){return 0},_pthread_getspecific:function(a){return $b[a]||0},_pthread_key_create:function(a){if(0==a)return H.h;D[a>>2]=ac;$b[ac]=0;ac++;return 0},_pthread_once:bc,_pthread_setspecific:function(a,b){if(!(a in $b))return H.h;$b[a]=b;return 0},DYNAMICTOP_PTR:E,STACKTOP:Ba};var dc=e.asm(e.Aa,e.Ba,buffer);
e.asm=dc;var Va=e.__GLOBAL__sub_I_adplug_cpp=function(){return e.asm.__GLOBAL__sub_I_adplug_cpp.apply(null,arguments)},cc=e.__ZSt18uncaught_exceptionv=function(){return e.asm.__ZSt18uncaught_exceptionv.apply(null,arguments)};e.___errno_location=function(){return e.asm.___errno_location.apply(null,arguments)};e._emu_compute_audio_samples=function(){return e.asm._emu_compute_audio_samples.apply(null,arguments)};e._emu_get_audio_buffer=function(){return e.asm._emu_get_audio_buffer.apply(null,arguments)};
e._emu_get_audio_buffer_length=function(){return e.asm._emu_get_audio_buffer_length.apply(null,arguments)};e._emu_get_current_position=function(){return e.asm._emu_get_current_position.apply(null,arguments)};e._emu_get_inst_text=function(){return e.asm._emu_get_inst_text.apply(null,arguments)};e._emu_get_max_position=function(){return e.asm._emu_get_max_position.apply(null,arguments)};e._emu_get_num_insts=function(){return e.asm._emu_get_num_insts.apply(null,arguments)};
e._emu_get_sample_rate=function(){return e.asm._emu_get_sample_rate.apply(null,arguments)};e._emu_get_trace_streams=function(){return e.asm._emu_get_trace_streams.apply(null,arguments)};e._emu_get_trace_titles=function(){return e.asm._emu_get_trace_titles.apply(null,arguments)};e._emu_get_track_info=function(){return e.asm._emu_get_track_info.apply(null,arguments)};e._emu_load_file=function(){return e.asm._emu_load_file.apply(null,arguments)};
e._emu_number_trace_streams=function(){return e.asm._emu_number_trace_streams.apply(null,arguments)};e._emu_seek_position=function(){return e.asm._emu_seek_position.apply(null,arguments)};e._emu_set_options=function(){return e.asm._emu_set_options.apply(null,arguments)};e._emu_set_subsong=function(){return e.asm._emu_set_subsong.apply(null,arguments)};e._emu_teardown=function(){return e.asm._emu_teardown.apply(null,arguments)};e._free=function(){return e.asm._free.apply(null,arguments)};
var nb=e._malloc=function(){return e.asm._malloc.apply(null,arguments)},na=e.stackAlloc=function(){return e.asm.stackAlloc.apply(null,arguments)},ma=e.stackRestore=function(){return e.asm.stackRestore.apply(null,arguments)},la=e.stackSave=function(){return e.asm.stackSave.apply(null,arguments)};e.dynCall_v=function(){return e.asm.dynCall_v.apply(null,arguments)};e.dynCall_vi=function(){return e.asm.dynCall_vi.apply(null,arguments)};e.asm=dc;
e.ccall=function(a,b,c,d){var f=e["_"+a];assert(f,"Cannot call unknown function "+a+", make sure it is exported");var g=[];a=0;if(d)for(var h=0;h<d.length;h++){var m=ra[c[h]];m?(0===a&&(a=la()),g[h]=m(d[h])):g[h]=d[h]}c=f.apply(null,g);c="string"===b?A(c):"boolean"===b?!!c:c;0!==a&&ma(a);return c};
e.getValue=function(a,b){b=b||"i8";"*"===b.charAt(b.length-1)&&(b="i32");switch(b){case "i1":return y[a>>0];case "i8":return y[a>>0];case "i16":return ua[a>>1];case "i32":return D[a>>2];case "i64":return D[a>>2];case "float":return va[a>>2];case "double":return wa[a>>3];default:u("invalid type for getValue: "+b)}return null};e.getMemory=function(a){if(za)if(Oa)var b=nb(a);else{b=D[E>>2];a=b+a+15&-16;D[E>>2]=a;if(a=a>=F)Ea(),a=!0;a&&(D[E>>2]=b,b=0)}else b=ha(a);return b};e.Pointer_stringify=A;
e.UTF8ToString=sa;e.addRunDependency=Sa;e.removeRunDependency=Ta;e.FS_createFolder=Rb;e.FS_createPath=Sb;e.FS_createDataFile=Ub;e.FS_createPreloadedFile=Yb;e.FS_createLazyFile=Xb;e.FS_createLink=Vb;e.FS_createDevice=W;e.FS_unlink=Ib;Ra=function ec(){e.calledRun||fc();e.calledRun||(Ra=ec)};
function fc(){function a(){if(!e.calledRun&&(e.calledRun=!0,!ka)){Oa||(Oa=!0,Ga(Ia));Ga(Ja);if(e.onRuntimeInitialized)e.onRuntimeInitialized();if(e.postRun)for("function"==typeof e.postRun&&(e.postRun=[e.postRun]);e.postRun.length;){var a=e.postRun.shift();La.unshift(a)}Ga(La)}}if(!(0<G)){if(e.preRun)for("function"==typeof e.preRun&&(e.preRun=[e.preRun]);e.preRun.length;)Pa();Ga(Ha);0<G||e.calledRun||(e.setStatus?(e.setStatus("Running..."),setTimeout(function(){setTimeout(function(){e.setStatus("")},
1);a()},1)):a())}}e.run=fc;function u(a){if(e.onAbort)e.onAbort(a);void 0!==a?(fa(a),w(a),a=JSON.stringify(a)):a="";ka=!0;throw"abort("+a+"). Build with -s ASSERTIONS=1 for more info.";}e.abort=u;if(e.preInit)for("function"==typeof e.preInit&&(e.preInit=[e.preInit]);0<e.preInit.length;)e.preInit.pop()();e.noExitRuntime=!0;fc();
  return {
	Module: Module,  // expose original Module
  };
})(window.spp_backend_state_ADPLUG);
/*
 adplug_adapter.js: Adapts AdPlug backend to generic WebAudio/ScriptProcessor player.

 version 1.1

 	Copyright (C) 2015-2023 Juergen Wothke


 note: scope output is always enabled for this backend


 LICENSE

 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version. This library is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/
class AdPlugBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor()
	{
		super(backend_AdPlug.Module, 2,
				new SimpleFileMapper(backend_AdPlug.Module),
				// scope input range is 16bit but with 9 or even 18 channels (which are added to create the final signal)
				// only a subrange is typically used (to avoid overflows) and the used scaling seems about OK to create
				// graphs..
				new HEAP32ScopeProvider(backend_AdPlug.Module, 0x2000));


		this.codeMap = [	// codepage 437 used by PC DOS and MS-DOS
			0x0000,0x0001,0x0002,0x0003,0x0004,0x0005,0x0006,0x0007,
			0x0008,0x0009,0x000a,0x000b,0x000c,0x000d,0x000e,0x000f,
			0x0010,0x0011,0x0012,0x0013,0x0014,0x0015,0x0016,0x0017,
			0x0018,0x0019,0x001a,0x001b,0x001c,0x001d,0x001e,0x001f,
			0x0020,0x0021,0x0022,0x0023,0x0024,0x0025,0x0026,0x0027,
			0x0028,0x0029,0x002a,0x002b,0x002c,0x002d,0x002e,0x002f,
			0x0030,0x0031,0x0032,0x0033,0x0034,0x0035,0x0036,0x0037,
			0x0038,0x0039,0x003a,0x003b,0x003c,0x003d,0x003e,0x003f,
			0x0040,0x0041,0x0042,0x0043,0x0044,0x0045,0x0046,0x0047,
			0x0048,0x0049,0x004a,0x004b,0x004c,0x004d,0x004e,0x004f,
			0x0050,0x0051,0x0052,0x0053,0x0054,0x0055,0x0056,0x0057,
			0x0058,0x0059,0x005a,0x005b,0x005c,0x005d,0x005e,0x005f,
			0x0060,0x0061,0x0062,0x0063,0x0064,0x0065,0x0066,0x0067,
			0x0068,0x0069,0x006a,0x006b,0x006c,0x006d,0x006e,0x006f,
			0x0070,0x0071,0x0072,0x0073,0x0074,0x0075,0x0076,0x0077,
			0x0078,0x0079,0x007a,0x007b,0x007c,0x007d,0x007e,0x007f,
			0x00c7,0x00fc,0x00e9,0x00e2,0x00e4,0x00e0,0x00e5,0x00e7,
			0x00ea,0x00eb,0x00e8,0x00ef,0x00ee,0x00ec,0x00c4,0x00c5,
			0x00c9,0x00e6,0x00c6,0x00f4,0x00f6,0x00f2,0x00fb,0x00f9,
			0x00ff,0x00d6,0x00dc,0x00a2,0x00a3,0x00a5,0x20a7,0x0192,
			0x00e1,0x00ed,0x00f3,0x00fa,0x00f1,0x00d1,0x00aa,0x00ba,
			0x00bf,0x2310,0x00ac,0x00bd,0x00bc,0x00a1,0x00ab,0x00bb,
			0x2591,0x2592,0x2593,0x2502,0x2524,0x2561,0x2562,0x2556,
			0x2555,0x2563,0x2551,0x2557,0x255d,0x255c,0x255b,0x2510,
			0x2514,0x2534,0x252c,0x251c,0x2500,0x253c,0x255e,0x255f,
			0x255a,0x2554,0x2569,0x2566,0x2560,0x2550,0x256c,0x2567,
			0x2568,0x2564,0x2565,0x2559,0x2558,0x2552,0x2553,0x256b,
			0x256a,0x2518,0x250c,0x2588,0x2584,0x258c,0x2590,0x2580,
			0x03b1,0x00df,0x0393,0x03c0,0x03a3,0x03c3,0x00b5,0x03c4,
			0x03a6,0x0398,0x03a9,0x03b4,0x221e,0x03c6,0x03b5,0x2229,
			0x2261,0x00b1,0x2265,0x2264,0x2320,0x2321,0x00f7,0x2248,
			0x00b0,0x2219,0x00b7,0x221a,0x207f,0x00b2,0x25a0,0x00a0
		];
		
		this.setProcessorBufSize(4096);

		this.ensureReadyNotification();
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		let ret = this.Module.ccall('emu_load_file', 'number', ['string', 'number', 'number', 'number', 'number', 'number'],
													[filename, 0, 0, ScriptNodePlayer.getWebAudioSampleRate(), 1024, true]);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let endless = (typeof options.endless == 'undefined') ? 0 : (options.endless == true ? 1 : 0);
		let loops = (typeof options.loops == 'undefined') ? 1 : options.loops;
		this.Module.ccall('emu_set_options', 'number', ['number', 'number'], [endless, loops]);

		let track= options.track;
		return this.Module.ccall('emu_set_subsong', 'number', ['number'], [track]);
	}

	getSongInfoMeta()
	{
		return {
			title: String,
			author: String,
			desc: String,
			player: String,
			speed: Number,
			tracks: Number,
			instruments: String
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 7;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

		result.title = this.cp437ToString(array[0]);
		if (!result.title.length) result.title = this._makeTitleFromPath(filename);

		result.author = this.cp437ToString(array[1]);
		result.desc = this.Module.Pointer_stringify(array[2]);
		result.player = this.Module.Pointer_stringify(array[3]);

		let s = parseInt(this.Module.Pointer_stringify(array[4]))
		result.speed = s;

		let t = parseInt(this.Module.Pointer_stringify(array[5]))
		result.tracks = t;

		let instruments = "";
		let n = this.Module.ccall('emu_get_num_insts', 'number');
		for (let i = 0; i < n; i++) {
			let txt = this.Module.ccall('emu_get_inst_text', 'number', ['number'], [i]);
			instruments += this.cp437ToString(txt) + "<br>";
		}
		result.instruments = instruments;
	}

	cp437ToString(ptr) 	// msdos text to unicode..
	{
		let str = '';
		while (1) {
			let ch = this.Module.getValue(ptr++, 'i8', true);
			if (!ch) return str;
			str += String.fromCharCode(this.codeMap[ch& 0xff]);
		}
	}
};
